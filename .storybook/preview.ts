import React from 'react';
import type { Preview } from '@storybook/react';
import { initialize, mswDecorator } from 'msw-storybook-addon';

import '../src/assets/styles/style.scss';

initialize({
  onUnhandledRequest: 'bypass',
});

export const decorators = [mswDecorator];

const preview: Preview = {
  parameters: {
    options: {
      storySort: {
        order: ['Modules', 'Components', 'UI'],
      },
    },
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      sort: 'requiredFirst',
      expanded: true,
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;
