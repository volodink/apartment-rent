import { createBrowserRouter } from 'react-router-dom';

import { DEFAULT_PATH, DETAIL_PATH } from './paths';

import { Catalog } from '@modules/Catalog';
import { Detail } from '@modules/Detail';

export const router = createBrowserRouter([
  {
    path: DEFAULT_PATH,
    element: <Catalog />,
  },
  {
    path: `${DETAIL_PATH}/:id`,
    element: <Detail />,
  },
]);
