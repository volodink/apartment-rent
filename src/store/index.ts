import {
  PreloadedState,
  combineReducers,
  configureStore,
} from '@reduxjs/toolkit';

import { catalogApi, catalogReducer, catalogSlice } from '@modules/Catalog';
import { detailApi } from '@modules/Detail';

const rootReducers = combineReducers({
  [catalogSlice.name]: catalogReducer,
  [catalogApi.reducerPath]: catalogApi.reducer,
  [detailApi.reducerPath]: detailApi.reducer,
});

export const setupStore = (preloadedState?: PreloadedState<RootState>) => {
  return configureStore({
    reducer: rootReducers,
    preloadedState,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({ serializableCheck: false }).concat([
        catalogApi.middleware,
        detailApi.middleware,
      ]),
  });
};

export type RootState = ReturnType<typeof rootReducers>;
export type AppStore = ReturnType<typeof setupStore>;
