import type { Meta, StoryObj } from '@storybook/react';
import { useArgs } from '@storybook/preview-api';
import { action } from '@storybook/addon-actions';

import { INumberRange, NumberRange } from './NumberRange';

/** This component is used to specify the sampling interval. */
const meta: Meta<typeof NumberRange> = {
  title: 'Components/NumberRange',
  component: NumberRange,
  parameters: {
    layout: 'centered',
  },
  args: {
    postfix: '$',
  },
  tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  render: function Render(args) {
    const [{ min, max }, updateArgs] = useArgs();

    const handleChange = (range: INumberRange) => {
      action('onChange')(range);
      updateArgs({ min: range.min, max: range.max });
    };

    return (
      <NumberRange {...args} min={min} max={max} onChange={handleChange} />
    );
  },
};
