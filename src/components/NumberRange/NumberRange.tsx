import clsx from 'clsx';
import { useState, ReactNode, ChangeEvent } from 'react';

import { InputGroup } from '@components/InputGroup/InputGroup';

import styles from './NumberRange.module.scss';

export interface INumberRange {
  /** Sets the maximum value */
  max: number | undefined;
  /** Sets the minimum value */
  min: number;
}

interface INumberRangeProps extends INumberRange {
  /** The callback that will be called when changing and blur input */
  onChange: (value: INumberRange) => void;
  /** Sets a postfix for each input */
  postfix: ReactNode;
  /** Additional class for the component */
  className?: string;
}

export const NumberRange = ({
  min,
  max,
  onChange,
  postfix,
  className = '',
}: INumberRangeProps) => {
  const [range, setRange] = useState({ min, max });

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const regex = /^[0-9\b]+$/;
    const inputValue = event.target.value;
    const key = event.target.name;

    if (inputValue === '' || regex.test(inputValue)) {
      setRange((prev) => ({ ...prev, [key]: +inputValue }));
      onChange({ ...range, [key]: +inputValue });
    }
  };

  const handleBlur = () => {
    if (
      range.max &&
      range.min &&
      (range.min > range.max || range.max < range.min)
    ) {
      onChange({ ...range, max: range.min });
      setRange((prev) => ({ ...prev, max: prev.min }));
    }
  };

  return (
    <div className={clsx(styles.container, className)}>
      <InputGroup
        value={min ? range.min : ''}
        onChange={handleChange}
        onBlur={handleBlur}
        postfix={postfix}
        placeholder="От"
        name="min"
        autoComplete="off"
      />

      <InputGroup
        value={max ? range.max : ''}
        onChange={handleChange}
        onBlur={handleBlur}
        postfix={postfix}
        placeholder="До"
        name="max"
        autoComplete="off"
      />
    </div>
  );
};
