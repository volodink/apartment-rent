import { fireEvent, screen, render } from '@testing-library/react';
import { vi } from 'vitest';

import { NumberRange } from './NumberRange';

describe('NumberRange', () => {
  it('should render correctly', () => {
    const handleChange = vi.fn();
    render(
      <NumberRange min={0} max={10000} onChange={handleChange} postfix="$" />,
    );
    const rangeMinElement = screen.getByPlaceholderText(/От/i);
    const rangeMaxElement = screen.getByDisplayValue(/10000/i);

    expect(rangeMinElement).toBeInTheDocument();
    expect(rangeMaxElement).toBeInTheDocument();
  });

  it('should range change correctly', () => {
    const handleChange = vi.fn();
    render(
      <NumberRange
        min={2000}
        max={10000}
        onChange={handleChange}
        postfix="$"
      />,
    );
    const rangeMinElement = screen.getByDisplayValue<HTMLInputElement>(/2000/i);
    const rangeMaxElement =
      screen.getByDisplayValue<HTMLInputElement>(/10000/i);

    fireEvent.change(rangeMinElement, { target: { value: '1000' } });
    fireEvent.change(rangeMaxElement, { target: { value: '25000' } });

    expect(rangeMinElement.value).toBe('1000');
    expect(rangeMaxElement.value).toBe('25000');

    expect(handleChange).toBeCalledTimes(2);
  });

  it('should maximum price change after blur if it is less than the minimum', () => {
    const handleChange = vi.fn();
    render(
      <NumberRange
        min={2000}
        max={10000}
        onChange={handleChange}
        postfix="$"
      />,
    );
    const rangeMaxElement =
      screen.getByDisplayValue<HTMLInputElement>(/10000/i);

    fireEvent.change(rangeMaxElement, { target: { value: '500' } });

    fireEvent.blur(rangeMaxElement);

    expect(rangeMaxElement.value).toBe('2000');
    expect(handleChange).toBeCalledTimes(2);
  });

  it('should maximum price after blur change if the minimum is greater', () => {
    const handleChange = vi.fn();
    render(
      <NumberRange
        min={2000}
        max={10000}
        onChange={handleChange}
        postfix="$"
      />,
    );
    const rangeMinElement = screen.getByDisplayValue<HTMLInputElement>(/2000/i);
    const rangeMaxElement =
      screen.getByDisplayValue<HTMLInputElement>(/10000/i);

    fireEvent.change(rangeMinElement, { target: { value: '12000' } });
    fireEvent.blur(rangeMinElement);

    expect(rangeMaxElement.value).toBe('12000');
    expect(rangeMinElement.value).toBe('12000');
    expect(handleChange).toBeCalledTimes(2);
  });

  it('should match snapshot', () => {
    const handleChange = vi.fn();
    const container = render(
      <NumberRange
        min={2000}
        max={10000}
        onChange={handleChange}
        postfix="$"
      />,
    );

    expect(container).toMatchSnapshot();
  });
});
