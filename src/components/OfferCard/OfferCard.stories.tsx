import type { Meta, StoryObj } from '@storybook/react';

import { OfferCard } from './OfferCard';
import { Icon } from '@ui/Icon/Icon';

/** This component is used to display a description or benefits. */
const meta = {
  title: 'Components/OfferCard',
  component: OfferCard,
  parameters: {
    layout: 'centered',
  },
  args: {
    title: 'Quantity:',
    text: '12',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof OfferCard>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  render: (args) => (
    <OfferCard {...args} icon={<Icon name="square" size={24} />} />
  ),
};
