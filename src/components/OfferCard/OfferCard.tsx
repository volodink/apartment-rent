import { ReactNode } from 'react';

import styles from './OfferCard.module.scss';

interface IOfferCardProps {
  /** Sets the top title of the component*/
  title: string | number;
  /** Sets the bottom text of the component */
  text: string | number;
  /** Sets the displayed icon */
  icon?: ReactNode;
}

export const OfferCard = ({ icon, title, text }: IOfferCardProps) => {
  return (
    <div className={styles.container}>
      <div className={styles.icon}>{icon}</div>

      <div className={styles.content}>
        <h3 className={styles.title}>{title}</h3>
        <p className={styles.text}>{text}</p>
      </div>
    </div>
  );
};
