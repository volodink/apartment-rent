import { render, screen, RenderResult } from '@testing-library/react';

import { OfferCard } from './OfferCard';

describe('OfferCard', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = render(<OfferCard text="Text" title="Title" icon="Icon" />);
  });

  it('should render correctly', () => {
    const textElement = screen.getByText(/text/i);
    const titleElement = screen.getByText(/title/i);
    const iconElement = screen.getByText(/icon/i);

    expect(textElement).toBeInTheDocument();
    expect(titleElement).toBeInTheDocument();
    expect(iconElement).toBeInTheDocument();
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
