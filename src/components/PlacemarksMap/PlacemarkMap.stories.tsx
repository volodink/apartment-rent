import type { Meta, StoryObj } from '@storybook/react';
import { YMaps, Map } from '@pbe/react-yandex-maps';

import { MOCK_DATA_PLACEMARKS } from '@tests/mocks/placemarkMock';
import {
  placemarkTransformText,
  clusterTransformText,
} from '@modules/Catalog/utils';

import { IApartmentPlacemark } from '@modules/Catalog/interfaces';

import { PlacemarksMap } from './PlacemarksMap';

/** This component can be used to add placemarks to Yandex Map. */
const meta: Meta<
  typeof PlacemarksMap<IApartmentPlacemark, keyof IApartmentPlacemark>
> = {
  title: 'Components/PlacemarksMap',
  component: PlacemarksMap,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  argTypes: {
    onPlacemarkClick: { action: 'onPlacemarkClick' },
  },
  args: {
    data: MOCK_DATA_PLACEMARKS,
    displayedMarkValue: 'price',
    transformClusterText: clusterTransformText,
    transformMarkText: placemarkTransformText,
  },
  decorators: [
    (Story) => (
      <YMaps>
        <Map
          defaultState={{
            center: [55.751574, 37.573856],
            zoom: 10,
          }}
          width="1200px"
          height="720px"
        >
          <Story />
        </Map>
      </YMaps>
    ),
  ],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
