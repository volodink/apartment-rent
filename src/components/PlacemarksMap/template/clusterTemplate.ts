export const clusterTemplate = () => {
  return `
    <div class="w-24 h-7 font-mont text-sm font-medium relative flex items-center justify-center bg-white text-slate-900 shadow-lg rounded-md border border-gray-300">
      {{ properties.geoObjects|transformClusterText }}
    </div>
  `;
};
