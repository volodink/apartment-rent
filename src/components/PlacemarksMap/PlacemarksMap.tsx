/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-nocheck

import { useEffect, useMemo, useRef } from 'react';
import { ObjectManager, useYMaps } from '@pbe/react-yandex-maps';
import ymaps from 'yandex-maps';

import { placemarkTemplate } from './template/placemarkTemplate';
import { clusterTemplate } from './template/clusterTemplate';

interface IPlacemarksMapProps<T, K extends keyof T> {
  /** Sets the displayed data on the map */
  data: T[];
  /** Sets the visibility of data loading */
  isLoading?: boolean;
  /** Sets the value to be displayed on the placemarks */
  displayedMarkValue: K;
  /** Callback that will be called when you click on placemark or cluster */
  onPlacemarkClick: (array: T[]) => void;
  /** A callback that converts the cluster content */
  transformClusterText: (data: T[]) => string;
  /** A callback that converts the placemark content */
  transformMarkText: (data: string | number) => string;
}

export const PlacemarksMap = <T, K extends keyof T>({
  data,
  isLoading = false,
  displayedMarkValue,
  transformMarkText,
  onPlacemarkClick,
  transformClusterText,
}: IPlacemarksMapProps<T, K>) => {
  const ref = useRef<ymaps.ObjectManager>();
  const userMap = useYMaps([
    'templateLayoutFactory',
    'template.filtersStorage',
  ]);

  const placemarkLayout = useMemo(() => {
    if (!userMap) return '';

    const layout = userMap.templateLayoutFactory.createClass(
      placemarkTemplate(displayedMarkValue as string),
    );

    return layout;
  }, [userMap]);

  const clusterLayout = useMemo(() => {
    if (!userMap) return '';

    const layout = userMap.templateLayoutFactory.createClass(clusterTemplate());

    return layout;
  }, [userMap]);

  const handleClick = (event: ymaps.MapEvent) => {
    const objectId = event.get('objectId');
    const clusterObject = ref.current?.clusters.getById(objectId);

    if (clusterObject) {
      onPlacemarkClick(clusterObject.features);
    } else {
      onPlacemarkClick([ref.current?.objects.getById(objectId) as T]);
    }
  };

  useEffect(() => {
    userMap?.template.filtersStorage.add(
      'transformMarkText',
      (_, value: string | number) => transformMarkText(value),
    );

    userMap?.template.filtersStorage.add(
      'transformClusterText',
      (_, value: T[]) => transformClusterText(value),
    );
  }, [userMap]);

  if (isLoading) {
    return;
  }

  return (
    <ObjectManager
      options={{
        clusterize: true,
        gridSize: 32,
        groupByCoordinates: true,
        clusterDisableClickZoom: true,
      }}
      clusters={{
        clusterIconLayout: clusterLayout,
        iconShape: {
          type: 'Rectangle',
          coordinates: [
            [0, 0],
            [96, 28],
          ],
        },
      }}
      objects={{
        iconLayout: placemarkLayout,
        iconShape: {
          type: 'Rectangle',
          coordinates: [
            [0, 0],
            [80, 28],
          ],
        },
      }}
      features={data}
      instanceRef={ref}
      onClick={handleClick}
    />
  );
};
