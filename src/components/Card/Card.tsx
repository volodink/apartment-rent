import clsx from 'clsx';
import { Link } from 'react-router-dom';

import styles from './Card.module.scss';

interface ICardProps {
  /** Sets a unique id */
  id: number;
  /** Sets the link to the image */
  image: string;
  /** Sets the number of rooms */
  rooms: number;
  /** Sets the area of the apartment */
  square: number;
  /** Sets the price of the apartment */
  price: number;
  /** Sets the address of the apartment */
  address: string;
  /** Additional class for the component */
  className?: string;
}

export const Card = ({
  id,
  image,
  rooms,
  square,
  price,
  address,
  className = '',
}: ICardProps) => {
  return (
    <div className={clsx(styles.container, className)}>
      <Link
        to={`apartment/${id}`}
        className={styles.image}
        target="_blank"
        rel="noopener noreferrer"
      >
        <img alt="Комната" src={image} />
      </Link>

      <div className={styles.content}>
        <Link
          to={`apartment/${id}`}
          className={styles.information}
          target="_blank"
          rel="noopener noreferrer"
        >
          {rooms}-к. квартира, {square} м²
        </Link>
        <p className={styles.price}>{price.toLocaleString()} ₽ за сутки</p>
        <p className={styles.address}>{address}</p>
      </div>
    </div>
  );
};
