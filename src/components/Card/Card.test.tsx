import { screen, RenderResult } from '@testing-library/react';
import { renderWithRouter } from '@tests/helpers/renderWithRouter';

import { Card } from './Card';

describe('Card', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = renderWithRouter(
      <Card
        id={1}
        image="./test.png"
        address="Address"
        rooms={3}
        square={43}
        price={12000}
      />,
    );
  });

  it('should render correctly', () => {
    const imageElement = screen.getByAltText<HTMLImageElement>(/Комната/i);
    const linkElement = screen.getByText<HTMLLinkElement>(/3-к. квартира/i);
    const priceElement = screen.getByText(/12 000/i);

    expect(imageElement).toHaveAttribute('src', './test.png');
    expect(linkElement).toBeInTheDocument();
    expect(priceElement).toBeInTheDocument();
    expect(linkElement).toHaveAttribute('href', '/apartment/1');
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
