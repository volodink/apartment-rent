import type { Meta, StoryObj } from '@storybook/react';

import { SpoilerButton } from './SpoilerButton';

/** This component can be used to hide any text.*/
const meta = {
  title: 'Components/SpoilerButton',
  component: SpoilerButton,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  args: {
    renderVariant: (isClicked) => (isClicked ? 'secondary' : 'primary'),
    hiddenText: 'Hidden',
    children: 'Show',
  },
} satisfies Meta<typeof SpoilerButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
