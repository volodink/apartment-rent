import {
  render,
  screen,
  RenderResult,
  fireEvent,
} from '@testing-library/react';

import { SpoilerButton } from './SpoilerButton';

describe('SpoilerButton', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = render(
      <SpoilerButton
        hiddenText="Hidden"
        renderVariant={(isClicked) => (isClicked ? 'secondary' : 'primary')}
      >
        Show Text
      </SpoilerButton>,
    );
  });

  it('should render correctly', () => {
    const buttonElement = screen.getByText(/Show Text/i);
    const hiddenText = screen.queryByText(/Hidden/i);

    expect(buttonElement).toBeInTheDocument();
    expect(hiddenText).not.toBeInTheDocument();
  });

  it('should show hidden text after a click', () => {
    const buttonElement = screen.getByText(/Show Text/i);
    const hiddenText = screen.queryByText(/Hidden/i);

    expect(hiddenText).not.toBeInTheDocument();
    expect(buttonElement.getAttribute('class')).toMatch(/primary/gi);

    fireEvent.click(buttonElement);

    const visibleText = screen.getByText(/Hidden/i);
    expect(buttonElement.getAttribute('class')).toMatch(/secondary/gi);
    expect(visibleText).toBeInTheDocument();
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
