import { useState } from 'react';

import { Button, IButtonProps } from '@ui/Button/Button';

interface ISpoilerButtonProps extends IButtonProps {
  /** Sets the hidden text */
  hiddenText: string;
  /** Sets the scheme variant for the button */
  renderVariant: (value: boolean) => 'secondary' | 'primary';
}

export const SpoilerButton = ({
  children,
  hiddenText,
  renderVariant,
  ...rest
}: ISpoilerButtonProps) => {
  const [isClicked, setIsClicked] = useState(false);

  return (
    <Button
      variant={renderVariant(isClicked)}
      onClick={() => setIsClicked((prev) => !prev)}
      {...rest}
    >
      {isClicked ? hiddenText : children}
    </Button>
  );
};
