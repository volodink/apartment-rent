import { ReactNode, useEffect, useRef, useState } from 'react';
import clsx from 'clsx';

import { IInputProps, Input } from '@ui/Input/Input';

import styles from './InputGroup.module.scss';

interface IInputGroupProps extends IInputProps {
  /** Sets the displayed element on the left side */
  suffix?: ReactNode;
  /** Sets the displayed element on the right side */
  postfix?: ReactNode;
  /** Sets the padding for the input */
  defaultPadding?: number;
  /** Sets the gap for the suffix and postfix */
  defaultGap?: number;
}

interface PaddingElement {
  /** Left padding for input */
  left: number | string;
  /** Right padding for input */
  right: number | string;
}

export const InputGroup = ({
  suffix,
  postfix,
  defaultPadding = 12,
  defaultGap = 8,
  error,
  ...rest
}: IInputGroupProps) => {
  const suffixRef = useRef<HTMLDivElement>(null);
  const postfixRef = useRef<HTMLDivElement>(null);

  const [inputPadding, setInputPadding] = useState<PaddingElement>({
    left: '',
    right: '',
  });

  useEffect(() => {
    const suffixWidth = suffixRef.current?.offsetWidth;
    const postfixWidth = postfixRef.current?.offsetWidth;
    const elementsPadding = defaultGap + defaultPadding;

    setInputPadding({
      left: suffix && suffixWidth ? suffixWidth + elementsPadding : '',
      right: postfix && postfixWidth ? postfixWidth + elementsPadding : '',
    });
  }, [suffix, postfix]);

  return (
    <div className={clsx(styles.container, { [styles.error]: error })}>
      {suffix && (
        <div ref={suffixRef} className={clsx(styles.element, styles.suffix)}>
          {suffix}
        </div>
      )}

      <Input
        {...rest}
        error={error}
        className={styles.input}
        style={{
          paddingRight: inputPadding.right,
          paddingLeft: inputPadding.left,
        }}
      />

      {postfix && (
        <div ref={postfixRef} className={clsx(styles.element, styles.postfix)}>
          {postfix}
        </div>
      )}
    </div>
  );
};
