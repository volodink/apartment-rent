import { render, screen, RenderResult } from '@testing-library/react';

import { InputGroup } from './InputGroup';

describe('InputGroup', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = render(
      <InputGroup suffix="Suffix" postfix={<button>Test</button>} />,
    );
  });

  it('should render correctly', () => {
    const suffixElement = screen.getByText(/suffix/i);
    const postfixElement = screen.getByText(/test/i);

    expect(suffixElement).toBeInTheDocument();
    expect(postfixElement).toBeInTheDocument();
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
