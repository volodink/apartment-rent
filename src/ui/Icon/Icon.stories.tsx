import type { Meta, StoryObj } from '@storybook/react';

import { Icon } from './Icon';

/** This component is a basic svg icon. */
const meta: Meta<typeof Icon> = {
  title: 'UI/Icon',
  component: Icon,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  args: {
    size: 24,
    color: '#000000',
    strokeWidth: 1.5,
  },
  argTypes: {
    color: {
      control: {
        type: 'color',
      },
    },
  },
};

export default meta;
type Story = StoryObj<typeof meta>;

/** This state is used to default. */
export const Default: Story = {
  args: {
    name: 'arrow',
  },
};

/** A list of icons used in the project is displayed here. */
export const AllIcons: Story = {
  render: (args) => {
    return (
      <>
        <Icon {...args} name="close" />
        <Icon {...args} name="square" />
        <Icon {...args} name="buildYear" />
        <Icon {...args} name="ceiling" />
        <Icon {...args} name="filters" />
        <Icon {...args} name="chevron" />
        <Icon {...args} name="arrow" />
      </>
    );
  },
  decorators: [
    (Story) => (
      <div className="gap-8 inline-flex">
        <Story />
      </div>
    ),
  ],
};
