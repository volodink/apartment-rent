import type { Meta, StoryObj } from '@storybook/react';

import { Button } from './Button';

/** This component can be used to trigger an action or event when you click on it.*/
const meta = {
  title: 'UI/Button',
  component: Button,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  args: {
    variant: 'primary',
  },
  argTypes: {
    onClick: { action: 'onClick' },
    variant: {
      control: { type: 'select' },
    },
  },
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

/** This state is used to display the default. */
export const Default: Story = {
  args: {
    children: 'Primary',
  },
};

/** This state is used to display the secondary variant. */
export const Secondary: Story = {
  args: {
    children: 'Secondary',
    variant: 'secondary',
  },
};

/** This state is used to display the light variant. */
export const Light: Story = {
  args: {
    children: 'Light',
    variant: 'light',
  },
};

/** This state is used to display the transparent variant. */
export const Transparent: Story = {
  args: {
    children: 'Transparent',
    variant: 'transparent',
  },
};
