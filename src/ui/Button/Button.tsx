import { ButtonHTMLAttributes } from 'react';
import clsx from 'clsx';

import styles from './Button.module.scss';

export interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  /** Sets component variant scheme */
  variant?: 'primary' | 'secondary' | 'light' | 'transparent';
  /** The content of the component */
  children: React.ReactNode;
}

export const Button = ({
  variant = 'primary',
  className = '',
  children,
  disabled,
  ...rest
}: IButtonProps) => {
  return (
    <button
      className={clsx(styles.button, className, styles[variant], {
        [styles.disabled]: disabled,
      })}
      disabled={disabled}
      {...rest}
    >
      {children}
    </button>
  );
};
