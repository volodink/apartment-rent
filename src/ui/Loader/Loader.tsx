import clsx from 'clsx';
import styles from './Loader.module.scss';

interface ILoaderProps {
  /** Additional class for the component */
  className?: string;
}

export const Loader = ({ className = '' }: ILoaderProps) => {
  return (
    <div className={clsx(styles.loader, className)} data-testid="loader"></div>
  );
};
