import { fireEvent, screen, render } from '@testing-library/react';
import { vi } from 'vitest';

import { Modal } from './Modal';

describe('Modal', () => {
  it('should render correctly', () => {
    const handleClick = vi.fn();
    render(
      <Modal
        title="Header"
        content="Body"
        footer="footer"
        onClose={handleClick}
        visible={true}
      />,
    );

    const headerElement = screen.getByText(/header/i);
    const bodyElement = screen.getByText(/body/i);
    const footerElement = screen.getByText(/footer/i);

    expect(headerElement).toBeInTheDocument();
    expect(bodyElement).toBeInTheDocument();
    expect(footerElement).toBeInTheDocument();
  });

  it('should close after clicking the close button', () => {
    const handleClick = vi.fn();
    render(
      <Modal
        title="Header"
        content="Body"
        footer="footer"
        onClose={handleClick}
        visible={true}
      />,
    );

    const closeButtonElement = screen.getByTestId('modal-close');
    fireEvent.click(closeButtonElement);

    expect(handleClick).toBeCalled();
  });

  it('should close after pressing Escape', () => {
    const handleClick = vi.fn();
    render(
      <Modal
        title="Header"
        content="Body"
        footer="footer"
        onClose={handleClick}
        visible={true}
      />,
    );

    fireEvent.keyDown(document, { key: 'Escape', keyCode: 27 });
    expect(handleClick).toBeCalled();
  });

  it('should ignore the click on the background', () => {
    const handleClick = vi.fn();
    render(
      <Modal
        title="Header"
        content="Body"
        footer="footer"
        onClose={handleClick}
        visible={true}
      />,
    );

    const backgroundElement = screen.getByTestId('modal-background');
    fireEvent.click(backgroundElement);

    expect(handleClick).not.toBeCalled();
  });

  it('should match snapshot', () => {
    const handleClick = vi.fn();
    const container = render(
      <Modal
        title="Header"
        content="Body"
        footer="footer"
        onClose={handleClick}
        visible={true}
      />,
    );

    expect(container).toMatchSnapshot();
  });
});
