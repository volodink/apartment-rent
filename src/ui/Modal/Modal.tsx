import { useEffect } from 'react';
import clsx from 'clsx';

import { Icon } from '@ui/Icon/Icon';

import styles from './Modal.module.scss';

interface IModalProps {
  /** Sets the visibility of the component */
  visible?: boolean;
  /** Sets the animation of the appearance and hiding of the modal window */
  transition?: boolean;
  /** Sets the title of the modal window */
  title?: string;
  /** Body content of the modal window */
  content: React.ReactNode;
  /** Footer content of the modal window */
  footer?: React.ReactNode;
  /** The callback that will be called when the modal window is closed */
  onClose: () => void;
  /** Additional class for the component */
  className?: string;
}

export const Modal = ({
  visible = false,
  title = '',
  content = '',
  footer = '',
  onClose,
  transition = false,
  className = '',
}: IModalProps) => {
  const onKeyDown = (event: KeyboardEvent): void => {
    if (event.key === 'Escape' && visible) {
      onClose();
    }
  };

  useEffect(() => {
    visible
      ? (document.body.style.overflow = 'hidden')
      : (document.body.style.overflow = 'unset');

    document.addEventListener('keydown', onKeyDown);
    return () => document.removeEventListener('keydown', onKeyDown);
  }, [visible]);

  if (!visible) return null;

  return (
    <div
      className={clsx(styles.modal, {
        [styles.delete]: transition,
      })}
      data-testid="modal-background"
    >
      <div
        className={clsx(styles.background, className, {
          [styles.delete]: transition,
        })}
        onClick={(e) => e.stopPropagation()}
      >
        <div className={styles.header}>
          <h3 className={styles.title}>{title}</h3>
          <span
            className={styles.close}
            onClick={onClose}
            data-testid="modal-close"
          >
            <Icon name="close" size={21} />
          </span>
        </div>

        {content && (
          <div
            className={clsx(styles.body, {
              [styles.default]: footer,
            })}
          >
            <div className={styles.content}>{content}</div>
          </div>
        )}

        {footer && <div className={styles.footer}>{footer}</div>}
      </div>
    </div>
  );
};
