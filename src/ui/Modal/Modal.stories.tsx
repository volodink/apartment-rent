import type { Meta, StoryObj } from '@storybook/react';
import { useArgs } from '@storybook/preview-api';
import { action } from '@storybook/addon-actions';

import { Modal } from './Modal';
import { Button } from '@ui/Button/Button';

/** This component is used to display a modal dialog. */
const meta: Meta<typeof Modal> = {
  title: 'UI/Modal',
  component: Modal,
  parameters: {
    layout: 'padded',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  render: function Render() {
    const [{ visible }, updateArgs] = useArgs();

    const toggleVisible = () => {
      action('onChange')();
      updateArgs({ visible: !visible });
    };

    return (
      <div>
        <Modal
          title="Modal Window"
          content="Body Content"
          footer={<Button onClick={toggleVisible}>Close modal</Button>}
          visible={visible}
          onClose={toggleVisible}
        />

        <Button onClick={toggleVisible}>Open modal</Button>
      </div>
    );
  },
};
