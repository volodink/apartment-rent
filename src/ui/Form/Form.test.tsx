import { render, screen, RenderResult } from '@testing-library/react';

import { Form } from './Form';

describe('Form', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = render(<Form label="Test">Input</Form>);
  });

  it('should render correctly', () => {
    const formElement = screen.getByText(/test/i);
    const childElement = screen.getByText(/input/i);

    expect(formElement).toBeInTheDocument();
    expect(childElement).toBeInTheDocument();
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
