import type { Meta, StoryObj } from '@storybook/react';

import { Form } from './Form';
import { Input } from '@ui/Input/Input';

import { Default as DefaultInput } from '../Input/Input.stories';

/** This component is used to display labels on top of form elements. */
const meta: Meta<typeof Form> = {
  title: 'UI/Form',
  component: Form,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    label: 'Label',
  },
  render: (args) => (
    <Form {...args}>
      <Input {...DefaultInput.args} />
    </Form>
  ),
};
