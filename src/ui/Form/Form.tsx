import { ReactNode } from 'react';

import styles from './Form.module.scss';

interface IFormProps {
  /** Sets the label of the children component */
  label: string;
  /** The content of the component */
  children: ReactNode;
}

export const Form = ({ label, children }: IFormProps) => {
  return (
    <div className={styles.container}>
      <label className={styles.label}>{label}</label>
      {children}
    </div>
  );
};
