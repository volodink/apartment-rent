import {
  fireEvent,
  screen,
  render,
  RenderResult,
} from '@testing-library/react';
import { vi } from 'vitest';

import { SelectItem } from './SelectItem';

describe('SelectItem', () => {
  let container: RenderResult;
  const handleClick = vi.fn();

  beforeEach(() => {
    container = render(
      <SelectItem value="test" onClick={handleClick}>
        Test
      </SelectItem>,
    );
  });

  it('should render correctly', () => {
    const selectItemElement = screen.getByText(/Test/i);
    expect(selectItemElement).toBeInTheDocument();
  });

  it('should handle click', () => {
    const selectItemElement = screen.getByText(/Test/i);
    fireEvent.click(selectItemElement);

    expect(handleClick).toHaveBeenCalled();
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
