import { HTMLAttributes, ReactNode } from 'react';
import clsx from 'clsx';

import styles from './SelectItem.module.scss';

interface ISelectItem extends HTMLAttributes<HTMLDivElement> {
  /** A unique value for a list item */
  value: string | number;
  /** Selects the active element */
  active?: boolean;
  /** The callback that will be called when the item is clicked */
  onClick?: () => void;
  /** The content of the component */
  children: ReactNode;
}

export const SelectItem = ({
  active = false,
  onClick,
  children,
  ...rest
}: ISelectItem) => {
  return (
    <div
      className={clsx(styles.item, {
        [styles.active]: active,
      })}
      onClick={onClick}
      {...rest}
    >
      {children}
    </div>
  );
};
