import type { Meta, StoryObj } from '@storybook/react';
import { useArgs } from '@storybook/preview-api';
import { action } from '@storybook/addon-actions';

import { MultipleSelectProps, Select, SelectOption } from './Select';
import { SelectItem } from '../SelectItem/SelectItem';
import { Unselected, Selected } from '../SelectItem/SelectItem.stories';

/** This component is used to select an item from the list of options. */
const meta: Meta<typeof Select> = {
  title: 'UI/Select',
  component: Select,
  parameters: {
    layout: 'centered',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof Select>;

/** This state is used to default. */
export const Default: Story = {
  render: function Render() {
    const [{ selectedValue }, updateArgs] = useArgs();

    const handleChange = (value: SelectOption) => {
      action('onChange')(value);
      updateArgs({ selectedValue: value });
    };

    return (
      <Select
        selectedValue={selectedValue}
        label={selectedValue?.label}
        onChange={handleChange}
      >
        <SelectItem {...Selected.args} value="item #1">
          Item #1
        </SelectItem>
        <SelectItem {...Unselected.args} value="item #2">
          Item #2
        </SelectItem>
        <SelectItem {...Unselected.args} value="item #3">
          Item #3
        </SelectItem>
      </Select>
    );
  },
};

/** This state is used to multiple selection. */
export const Multi: Story = {
  render: function Render() {
    const [{ selectedValue }, updateArgs] = useArgs<MultipleSelectProps>();

    const handleChange = (value: SelectOption[]) => {
      action('onChange')(value);
      updateArgs({ selectedValue: value });
    };

    return (
      <Select
        isMulti
        selectedValue={selectedValue || []}
        label={selectedValue?.map((element) => element.label).join(', ') || ''}
        onChange={handleChange}
      >
        <SelectItem {...Selected.args} value="item #1">
          Item #1
        </SelectItem>
        <SelectItem {...Unselected.args} value="item #2">
          Item #2
        </SelectItem>
        <SelectItem {...Unselected.args} value="item #3">
          Item #3
        </SelectItem>
      </Select>
    );
  },
};
