import { fireEvent, screen, render } from '@testing-library/react';
import { vi } from 'vitest';

import { Select } from './Select';

const MOCK_SELECT_ITEM = [
  {
    label: 'react',
    value: 'react',
  },
  {
    label: 'html',
    value: 'html',
  },
  {
    label: 'js',
    value: 'js',
  },
];

describe('Select', () => {
  it('should render correctly', () => {
    const handleClick = vi.fn();
    render(
      <Select
        selectedValue={MOCK_SELECT_ITEM[0]}
        label={MOCK_SELECT_ITEM[0].label}
        onChange={handleClick}
      >
        {MOCK_SELECT_ITEM.map((item) => (
          <li key={item.value} value={item.value}>
            {item.label}
          </li>
        ))}
      </Select>,
    );

    const selectHeader = screen.getByTestId('select-header');
    const selectBody = screen.queryByTestId('select-body');

    expect(selectHeader).toBeInTheDocument();
    expect(screen.queryByText(/react/i)).toBeInTheDocument();

    expect(selectBody).not.toBeInTheDocument();
  });

  it('should opens and closes without selecting an item', () => {
    const handleClick = vi.fn();
    render(
      <Select
        selectedValue={MOCK_SELECT_ITEM[0]}
        label={MOCK_SELECT_ITEM[0].label}
        onChange={handleClick}
      >
        {MOCK_SELECT_ITEM.map((item) => (
          <li key={item.value} value={item.value}>
            {item.label}
          </li>
        ))}
      </Select>,
    );

    const selectContainer = screen.getByTestId('select-container');
    const selectHeader = screen.getByTestId('select-header');

    fireEvent.click(selectHeader);

    const selectBody = screen.queryByTestId('select-body');

    expect(selectBody).toBeInTheDocument();
    expect(selectContainer.getAttribute('class')).toMatch(/active/gi);
    expect(screen.queryByText(/js/i)).toBeInTheDocument();

    fireEvent.click(document);

    expect(selectBody).not.toBeInTheDocument();
    expect(selectContainer.getAttribute('class')).not.toMatch(/active/gi);
  });

  it('should opens and closes with selecting an item', () => {
    const handleClick = vi.fn();
    render(
      <Select
        selectedValue={MOCK_SELECT_ITEM[0]}
        label={MOCK_SELECT_ITEM[0].label}
        onChange={handleClick}
      >
        {MOCK_SELECT_ITEM.map((item) => (
          <li key={item.value} value={item.value}>
            {item.label}
          </li>
        ))}
      </Select>,
    );

    const selectHeader = screen.getByTestId('select-header');

    fireEvent.click(selectHeader);

    const selectItem = screen.getByText(/js/i);

    fireEvent.click(selectItem);

    expect(handleClick).toHaveBeenCalledWith({
      label: 'js',
      value: 'js',
    });
    expect(selectItem).not.toBeInTheDocument();
  });

  it('should opens and closes with multi selecting an item', () => {
    const handleClick = vi.fn();
    render(
      <Select selectedValue={[]} onChange={handleClick} isMulti>
        {MOCK_SELECT_ITEM.map((item) => (
          <li key={item.value} value={item.value}>
            {item.label}
          </li>
        ))}
      </Select>,
    );

    const selectHeader = screen.getByTestId('select-header');

    fireEvent.click(selectHeader);

    const firstSelectItem = screen.getByText(/js/i);
    fireEvent.click(firstSelectItem);
    expect(handleClick).toHaveBeenCalledWith([
      {
        label: 'js',
        value: 'js',
      },
    ]);

    const secondSelectItem = screen.getByText(/react/i);
    fireEvent.click(secondSelectItem);
    expect(handleClick).toHaveBeenCalledWith([
      {
        label: 'react',
        value: 'react',
      },
    ]);

    fireEvent.click(document);

    expect(firstSelectItem).not.toBeInTheDocument();
    expect(secondSelectItem).not.toBeInTheDocument();

    expect(handleClick).toHaveBeenCalledTimes(2);
  });

  it('should match snapshot', () => {
    const handleClick = vi.fn();
    const container = render(
      <Select
        selectedValue={MOCK_SELECT_ITEM[0]}
        label={MOCK_SELECT_ITEM[0].label}
        onChange={handleClick}
      >
        {MOCK_SELECT_ITEM.map((item) => (
          <li key={item.value} value={item.value}>
            {item.label}
          </li>
        ))}
      </Select>,
    );

    expect(container).toMatchSnapshot();
  });
});
