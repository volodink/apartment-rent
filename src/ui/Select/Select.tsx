import {
  Children,
  ReactElement,
  cloneElement,
  isValidElement,
  ReactNode,
  useEffect,
  useRef,
  useState,
  MutableRefObject,
} from 'react';
import clsx from 'clsx';

import { Icon } from '../Icon/Icon';

import styles from './Select.module.scss';

export type SelectOption = {
  /** The displayed value of the element */
  label: string;
  /** The unique value of the element */
  value: string | number;
};

export type MultipleSelectProps = {
  /** Support multiple selected options */
  isMulti: true;
  /** Array of selected values from the list */
  selectedValue: SelectOption[];
  /** The callback that will be called when the menu item is selected */
  onChange: (value: SelectOption[]) => void;
};

export type SingleSelectProps = {
  /** Support multiple selected options */
  isMulti?: false;
  /** Selected value from the list */
  selectedValue?: SelectOption | undefined;
  /** The callback that will be called when the menu item is selected */
  onChange: (value: SelectOption) => void;
};

type SelectProps = {
  /** Sets the displayed values */
  label?: string;
  /** Sets the placeholder of the select component */
  placeholder?: string;
  /** Additional class for the component */
  className?: string;
  /** The content of the component */
  children: ReactNode;
} & (SingleSelectProps | MultipleSelectProps);

export const Select = ({
  isMulti,
  label,
  placeholder = 'Выбор данных',
  selectedValue,
  onChange,
  className = '',
  children,
}: SelectProps) => {
  const ref = useRef() as MutableRefObject<HTMLDivElement>;
  const [isOpen, setOpen] = useState(false);

  const handleChange = (item: SelectOption) => {
    if (isMulti) {
      if (selectedValue.some((x) => x.value === item.value)) {
        const newValues = selectedValue.filter(
          (option) => option.value !== item.value,
        );
        onChange(newValues);
      } else {
        const newValues = [...selectedValue, item];
        onChange(newValues);
      }
    } else {
      onChange(item);
      setOpen(false);
    }
  };

  const isItemSelected = (item: SelectOption) => {
    return isMulti
      ? selectedValue.some((x) => x.value === item.value)
      : selectedValue?.value === item.value;
  };

  useEffect(() => {
    const isClickedOutside = (event: MouseEvent) => {
      if (
        isOpen &&
        ref.current &&
        !ref.current.contains(event.target as Node)
      ) {
        setOpen(false);
      }
    };

    document.addEventListener('click', isClickedOutside);

    return () => {
      document.removeEventListener('click', isClickedOutside);
    };
  }, [isOpen]);

  return (
    <div className={clsx(styles.container, className)}>
      <div
        className={clsx(styles.select, {
          [styles.active]: isOpen,
        })}
        ref={ref}
        data-testid="select-container"
      >
        <button
          className={styles.title}
          onClick={() => setOpen(!isOpen)}
          data-testid="select-header"
        >
          {(isMulti && !selectedValue.length) || !label ? placeholder : label}
          <Icon name="chevron" size={8} className={styles.icon} />
        </button>

        {isOpen && (
          <div className={styles.menu} data-testid="select-body">
            {Children.map(children, (child) => {
              if (isValidElement(child)) {
                const option = {
                  value: child.props.value,
                  label: child.props.children,
                };

                return cloneElement(child as ReactElement, {
                  active: isItemSelected(option),
                  onClick: () => handleChange(option),
                });
              }
            })}
          </div>
        )}
      </div>
    </div>
  );
};
