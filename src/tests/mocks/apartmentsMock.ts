export const MOCK_DATA_APARTMENTS = [
  {
    id: 1,
    address: '1016 6th Park',
    description: 'Mini Description',
    phone: '+0 (000) 000-000-00',
    price: 15000,
    rooms: 4,
    square: 54,
    ceiling: 3,
    year: 1959,
    x: 55.751574,
    y: 37.573856,
    image:
      'https://images.unsplash.com/photo-1502672260266-1c1ef2d93688?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1980&q=80',
  },
  {
    id: 2,
    address: '93874 Green Park',
    description: 'Mini Description #2',
    phone: '+0 (000) 000-000-00',
    price: 12000,
    rooms: 1,
    square: 15,
    ceiling: 2,
    year: 1977,
    x: 55.751574,
    y: 37.573856,
    image:
      'https://images.unsplash.com/photo-1502672260266-1c1ef2d93688?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1980&q=80',
  },
];
