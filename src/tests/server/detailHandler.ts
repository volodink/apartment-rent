import { rest } from 'msw';

import { MOCK_DATA_DETAIL } from '@tests/mocks/detailMocks';

export const detailHandler = rest.get(
  `${import.meta.env.VITE_API_URL}/apartments/1`,
  (_req, res, ctx) => {
    return res(ctx.json(MOCK_DATA_DETAIL), ctx.status(200));
  },
);
