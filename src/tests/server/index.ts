import { setupServer } from 'msw/node';

import { apartmentHandler } from './apartmentHandler';

export const server = setupServer(...apartmentHandler);
