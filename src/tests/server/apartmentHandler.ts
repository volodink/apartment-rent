import { rest } from 'msw';

import { MOCK_DATA_DETAIL } from '@tests/mocks/detailMocks';
import { MOCK_DATA_APARTMENTS } from '@tests/mocks/apartmentsMock';

export const apartmentHandler = [
  rest.get(`${import.meta.env.VITE_API_URL}/apartments/1`, (_req, res, ctx) => {
    return res(ctx.json(MOCK_DATA_DETAIL), ctx.status(200));
  }),
  rest.get(`${import.meta.env.VITE_API_URL}/apartments`, (_req, res, ctx) => {
    return res(ctx.json(MOCK_DATA_APARTMENTS), ctx.status(200));
  }),
];
