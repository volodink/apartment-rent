export interface IApartment {
  id: number;
  address: string;
  description: string;
  phone: string;
  price: number;
  image: string;
  rooms: number;
  square: number;
  ceiling: number;
  year: number;
  x: number;
  y: number;
}
