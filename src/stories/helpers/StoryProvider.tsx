import { ReactNode } from 'react';
import { Provider } from 'react-redux';

import type { RenderOptions } from '@testing-library/react';
import type { PreloadedState } from '@reduxjs/toolkit';

import { RootState } from '@store/index';
import { AppStore, setupStore } from '@store/index';

interface ExtendedRenderOptions extends Omit<RenderOptions, 'queries'> {
  preloadedState?: PreloadedState<RootState>;
  store?: AppStore;
  children?: ReactNode;
}

export const StoryProvider = ({
  preloadedState = {},
  store = setupStore(preloadedState),
  children,
}: ExtendedRenderOptions = {}) => {
  return <Provider store={store}>{children}</Provider>;
};
