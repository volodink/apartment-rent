import { ReactNode } from 'react';

import type { RenderOptions } from '@testing-library/react';
import type { PreloadedState } from '@reduxjs/toolkit';

import { RootState, AppStore, setupStore } from '@store/index';
import { StoryRouter } from './StoryRouter';
import { StoryProvider } from './StoryProvider';

interface ExtendedRenderOptions extends Omit<RenderOptions, 'queries'> {
  preloadedState?: PreloadedState<RootState>;
  store?: AppStore;
  children?: ReactNode;
  initialEntries?: string;
}

export const StoryApp = ({
  preloadedState = {},
  store = setupStore(preloadedState),
  initialEntries = '',
  children,
}: ExtendedRenderOptions = {}) => {
  return (
    <StoryProvider store={store}>
      <StoryRouter initialEntries={initialEntries}>{children}</StoryRouter>
    </StoryProvider>
  );
};
