import { ReactNode } from 'react';
import { MemoryRouter } from 'react-router-dom';

interface IStoryRouterProps {
  children: ReactNode;
  initialEntries?: string;
}

export const StoryRouter = ({
  children,
  initialEntries = '/',
}: IStoryRouterProps) => {
  return (
    <MemoryRouter initialEntries={[initialEntries]}>{children}</MemoryRouter>
  );
};
