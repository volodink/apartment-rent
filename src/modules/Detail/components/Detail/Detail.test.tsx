import {
  screen,
  RenderResult,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import { Routes, Route } from 'react-router-dom';

import { DETAIL_PATH } from '@routes/paths';

import { server } from '@tests/server';
import { renderTestApp } from '@tests/helpers/renderTestApp';

import { Detail } from './Detail';

beforeAll(() => {
  server.listen();
});
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('Detail', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = renderTestApp(
      <Routes>
        <Route path={`${DETAIL_PATH}/:id`} element={<Detail />} />
      </Routes>,
      { initialEntries: [`${DETAIL_PATH}/1`] },
    );
  });

  it('should render correctly', async () => {
    await waitForElementToBeRemoved(screen.getByTestId('loader'));

    const imageElement = screen.getByAltText<HTMLImageElement>(/Комната/i);
    const nameElement =
      screen.getByText<HTMLLinkElement>(/4-комнатная квартира/i);
    const priceElement = screen.getByText(/15 000/i);

    const descriptionElement = screen.getByText(/mini description/i);
    const ceilingElement = screen.getByText(/3/i);
    const yearElement = screen.getByText(/1959/i);

    expect(imageElement).toHaveAttribute(
      'src',
      expect.stringContaining('unsplash'),
    );
    expect(nameElement).toBeInTheDocument();
    expect(priceElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
    expect(ceilingElement).toBeInTheDocument();
    expect(yearElement).toBeInTheDocument();
  });

  it('should match snapshot', async () => {
    await waitForElementToBeRemoved(screen.getByTestId('loader'));

    expect(container).toMatchSnapshot();
  });
});
