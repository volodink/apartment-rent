import type { Meta, StoryObj } from '@storybook/react';
import { Route, Routes } from 'react-router-dom';

import { DETAIL_PATH } from '@routes/paths';

import { apartmentHandler } from '@tests/server/apartmentHandler';
import { StoryApp } from '@stories/helpers/StoryApp';

import { Detail } from './Detail';

/** This module is used to display the description of the apartment. */
const meta = {
  title: 'Modules/Detail',
  component: Detail,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  decorators: [
    (Story) => (
      <StoryApp initialEntries={`${DETAIL_PATH}/1`}>
        <Routes>
          <Route path={`${DETAIL_PATH}/:id`} element={<Story />} />
        </Routes>
      </StoryApp>
    ),
  ],
} satisfies Meta<typeof Detail>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  parameters: {
    msw: apartmentHandler,
  },
};
