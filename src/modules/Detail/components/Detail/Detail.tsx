import { useParams } from 'react-router-dom';

import { useGetApartmentQuery } from '../../api/detailApi';

import { Loader } from '@ui/Loader/Loader';
import { Header } from '../Header/Header';
import { Information } from '../Information/Information';
import { Description } from '../Description/Description';

import styles from './Detail.module.scss';

export const Detail = () => {
  const { id } = useParams();

  const {
    data: apartment,
    isLoading,
    isError,
  } = useGetApartmentQuery(id as string);

  if (isLoading) {
    return (
      <div className={styles.loader}>
        <Loader />
      </div>
    );
  }

  if (!apartment || isError) {
    return 'Ошибка';
  }

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <div className={styles.image}>
          <img alt="Комната" src={apartment.image} />
        </div>
      </div>

      <Header />

      <Information
        address={apartment.address}
        price={apartment.price}
        rooms={apartment.rooms}
        square={apartment.square}
        phone={apartment.phone}
      />

      <div className={styles.information}>
        <Description
          square={apartment.square}
          ceiling={apartment.ceiling}
          year={apartment.year}
          description={apartment.description}
        />
      </div>
    </div>
  );
};
