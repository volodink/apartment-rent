import { Link } from 'react-router-dom';

import { Icon } from '@ui/Icon/Icon';

import styles from './Header.module.scss';

export const Header = () => {
  return (
    <div className={styles.container}>
      <div className={styles.inner}>
        <Link className={styles.back} to="/">
          <Icon name="arrow" size={24} />
        </Link>
      </div>
    </div>
  );
};
