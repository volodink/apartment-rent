import type { Meta, StoryObj } from '@storybook/react';

import { StoryRouter } from '@stories/helpers/StoryRouter';

import { Header } from './Header';

/** This component is used to show the available actions */
const meta = {
  title: 'Modules/Detail/Header',
  component: Header,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  decorators: [
    (Story) => (
      <StoryRouter>
        <Story />
      </StoryRouter>
    ),
  ],
} satisfies Meta<typeof Header>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  decorators: [
    (Story) => (
      <div className="relative">
        <Story />
      </div>
    ),
  ],
};
