import { screen, RenderResult } from '@testing-library/react';
import { renderWithRouter } from '@tests/helpers/renderWithRouter';

import { Header } from './Header';

describe('Header', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = renderWithRouter(<Header />);
  });

  it('should render correctly', () => {
    const linkElement = screen.getByRole<HTMLLinkElement>('link');

    expect(linkElement).toBeInTheDocument();
    expect(linkElement).toHaveAttribute('href', '/');
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
