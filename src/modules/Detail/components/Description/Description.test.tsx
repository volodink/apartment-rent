import { screen, RenderResult } from '@testing-library/react';
import { renderWithRouter } from '@tests/helpers/renderWithRouter';

import { Description } from './Description';

describe('Description', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = renderWithRouter(
      <Description ceiling={2.5} description="Desc" square={54} year={2023} />,
    );
  });

  it('should render correctly', () => {
    const descriptionElement = screen.getByText(/desc/i);
    const ceilingElement = screen.getByText(/2.5/i);
    const squareElement = screen.getByText(/54/i);
    const yearElement = screen.getByText(/2023/i);

    expect(descriptionElement).toBeInTheDocument();
    expect(ceilingElement).toBeInTheDocument();
    expect(squareElement).toBeInTheDocument();
    expect(yearElement).toBeInTheDocument();
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
