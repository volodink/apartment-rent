import { Icon } from '@ui/Icon/Icon';
import { OfferCard } from '@components/OfferCard/OfferCard';

import styles from './Description.module.scss';

interface IDescriptionProps {
  /** Sets the area of the apartment */
  square: number;
  /** Sets the year building of the apartment */
  year: number;
  /** Sets the ceiling height of the apartment */
  ceiling: number;
  /** Sets the description of the apartment */
  description: string;
}

export const Description = ({
  square,
  year,
  ceiling,
  description,
}: IDescriptionProps) => {
  return (
    <div className={styles.container}>
      <div className={styles.description}>
        <h2 className={styles.subtitle}>Описание</h2>

        <div className={styles.items}>
          <OfferCard
            icon={<Icon name="square" size={24} className={styles.icon} />}
            title="Общая площадь:"
            text={`${square} м²`}
          />
          <OfferCard
            icon={<Icon name="buildYear" size={24} className={styles.icon} />}
            title="Год постройки:"
            text={year}
          />
          <OfferCard
            icon={<Icon name="ceiling" size={24} className={styles.icon} />}
            title="Высота потолков:"
            text={`${ceiling} м`}
          />
        </div>
      </div>

      <p className={styles.text}>{description}</p>
    </div>
  );
};
