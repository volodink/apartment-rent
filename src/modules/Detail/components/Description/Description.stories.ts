import type { Meta, StoryObj } from '@storybook/react';

import { Description } from './Description';

/** This component is used to display detailed information about the apartment.*/
const meta = {
  title: 'Modules/Detail/Description',
  component: Description,
  parameters: {
    layout: 'padded',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof Description>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    year: 2023,
    ceiling: 2,
    square: 54,
    description: 'Mini Description',
  },
};
