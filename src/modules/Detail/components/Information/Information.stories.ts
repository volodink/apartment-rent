import type { Meta, StoryObj } from '@storybook/react';

import { Information } from './Information';

/** This component can be used to display information about the apartment.*/
const meta = {
  title: 'Modules/Detail/Information',
  component: Information,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
} satisfies Meta<typeof Information>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    address: '1016 6th Park',
    price: 15000,
    rooms: 3,
    square: 54,
    phone: '+0 (000) 000-000-00',
  },
};
