import { SpoilerButton } from '@components/SpoilerButton/SpoilerButton';

import styles from './Information.module.scss';

interface IInformationProps {
  /** Sets the number of rooms */
  rooms: number;
  /** Sets the area of the apartment */
  square: number;
  /** Sets the price of the apartment */
  price: number;
  /** Sets the address of the apartment */
  address: string;
  /** Sets the apartment owner's number */
  phone: string;
}

export const Information = ({
  rooms,
  square,
  address,
  price,
  phone,
}: IInformationProps) => {
  return (
    <div className={styles.container}>
      <h1 className={styles.information}>
        {rooms}-комнатная квартира, {square} м²
      </h1>
      <p className={styles.address}>{address}</p>
      <p className={styles.price}>{price.toLocaleString()} ₽ за сутки</p>

      <div className={styles.action}>
        <SpoilerButton
          hiddenText={phone}
          renderVariant={(isClicked) => (isClicked ? 'secondary' : 'primary')}
          className={styles.button}
        >
          Показать телефон
          <span className={styles.phone}> +7 (XXX) XXX-XX-XX</span>
        </SpoilerButton>
      </div>
    </div>
  );
};
