import {
  render,
  screen,
  RenderResult,
  fireEvent,
} from '@testing-library/react';

import { Information } from './Information';

describe('Information', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = render(
      <Information
        address="Address"
        rooms={3}
        square={43}
        price={12000}
        phone="+0 (000) 000-000-00"
      />,
    );
  });

  it('should render correctly', () => {
    const nameElement =
      screen.getByText<HTMLLinkElement>(/3-комнатная квартира/i);
    const priceElement = screen.getByText(/12 000/i);

    expect(nameElement).toBeInTheDocument();
    expect(priceElement).toBeInTheDocument();
  });

  it('should show hidden text after a click', () => {
    const buttonElement = screen.getByText(/Показать телефон/i);
    const hiddenText = screen.queryByText('+0 (000) 000-000-00');

    expect(hiddenText).not.toBeInTheDocument();
    expect(buttonElement.getAttribute('class')).toMatch(/primary/gi);

    fireEvent.click(buttonElement);

    const visibleText = screen.getByText('+0 (000) 000-000-00');
    expect(buttonElement.getAttribute('class')).toMatch(/secondary/gi);
    expect(visibleText).toBeInTheDocument();
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
