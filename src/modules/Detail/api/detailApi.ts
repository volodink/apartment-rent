import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { IApartment } from '@interfaces/Apartment';

export const detailApi = createApi({
  reducerPath: 'api/detail',
  baseQuery: fetchBaseQuery({
    baseUrl: `${import.meta.env.VITE_API_URL}`,
  }),
  endpoints: (build) => ({
    getApartment: build.query<IApartment, string>({
      query: (id) => ({
        url: `/apartments/${id}`,
      }),
    }),
  }),
});

export const { useGetApartmentQuery } = detailApi;
