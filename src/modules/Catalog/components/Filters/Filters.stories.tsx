import type { Meta, StoryObj } from '@storybook/react';

import { StoryApp } from '@stories/helpers/StoryApp';

import { Filters } from './Filters';

/** This component is used to filter placemarks on the map.*/
const meta = {
  title: 'Modules/Catalog/Filters',
  component: Filters,
  parameters: {
    layout: 'padded',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  tags: ['autodocs'],
  decorators: [
    (Story) => (
      <StoryApp>
        <Story />
      </StoryApp>
    ),
  ],
} satisfies Meta<typeof Filters>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
