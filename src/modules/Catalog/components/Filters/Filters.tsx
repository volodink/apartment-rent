import { useModal } from '@hooks/useModal';

import { Modal } from '@ui/Modal/Modal';
import { Icon } from '@ui/Icon/Icon';
import { Button } from '@ui/Button/Button';
import { FiltersList } from './FiltersList/FiltersList';

import styles from './Filters.module.scss';

export const Filters = () => {
  const { isVisible, toggleVisible, transition } = useModal();

  return (
    <>
      <Modal
        title="Фильтр"
        content={<FiltersList toggleVisible={() => toggleVisible()} />}
        visible={isVisible}
        onClose={toggleVisible}
        transition={transition}
      />

      <div className={styles.container}>
        <Button
          variant="light"
          onClick={toggleVisible}
          className={styles.button}
        >
          <Icon name="filters" size={20} />
          Фильтр
        </Button>
      </div>
    </>
  );
};
