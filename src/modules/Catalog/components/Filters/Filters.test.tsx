import {
  fireEvent,
  screen,
  waitFor,
  RenderResult,
} from '@testing-library/react';

import { renderWithProviders } from '@tests/helpers/renderWithProvider';

import { Filters } from './Filters';

describe('Filters', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = renderWithProviders(<Filters />);
  });

  it('should render correctly', () => {
    const buttonElement = screen.getByRole('button', { name: 'Фильтр' });
    const modalElement = screen.queryByTestId('modal-background');

    expect(buttonElement).toBeInTheDocument();
    expect(modalElement).not.toBeInTheDocument();
  });

  it('should open modal window correctly', () => {
    const buttonElement = screen.getByRole('button', { name: 'Фильтр' });
    const modalElement = screen.queryByTestId('modal-background');

    expect(modalElement).not.toBeInTheDocument();

    fireEvent.click(buttonElement);

    waitFor(() => expect(modalElement).toBeInTheDocument());
  });

  it('should close modal window correctly', () => {
    const buttonElement = screen.getByRole('button', { name: 'Фильтр' });
    const modalElement = screen.queryByTestId('modal-background');

    expect(modalElement).not.toBeInTheDocument();

    fireEvent.click(buttonElement);

    waitFor(() => expect(modalElement).toBeInTheDocument());
    const applyButtonElement = screen.getByTestId('filters-apply');

    fireEvent.click(applyButtonElement);
    waitFor(() => expect(modalElement).not.toBeInTheDocument());
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
