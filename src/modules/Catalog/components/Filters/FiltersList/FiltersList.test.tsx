import { fireEvent, screen } from '@testing-library/react';
import { vi } from 'vitest';

import { renderWithProviders } from '@tests/helpers/renderWithProvider';

import { FiltersList } from './FiltersList';

describe('FiltersList', () => {
  it('should render correctly', () => {
    const handleToggleVisible = vi.fn();
    renderWithProviders(<FiltersList toggleVisible={handleToggleVisible} />);

    const selectElement = screen.getByText(/любое/i);
    const rangeMinElement = screen.getByPlaceholderText(/от/i);
    const rangeMaxElement = screen.getByPlaceholderText(/до/i);

    expect(selectElement).toBeInTheDocument();
    expect(rangeMinElement).toBeInTheDocument();
    expect(rangeMaxElement).toBeInTheDocument();
  });

  it('should change number of rooms correctly', () => {
    const handleToggleVisible = vi.fn();
    renderWithProviders(<FiltersList toggleVisible={handleToggleVisible} />);

    const selectElement = screen.getByText(/любое/i);
    const buttonElement = screen.getByTestId('filters-apply');

    fireEvent.click(selectElement);

    const selectItem = screen.getByText(/1 комната/i);

    fireEvent.click(selectItem);

    const selectedItem = screen.getByText(/1 комната/i);
    expect(selectedItem).toBeInTheDocument();

    fireEvent.click(buttonElement);

    expect(handleToggleVisible).toBeCalled();
  });

  it('should change range price correctly', () => {
    const handleToggleVisible = vi.fn();
    renderWithProviders(<FiltersList toggleVisible={handleToggleVisible} />);

    const rangeMinElement =
      screen.getByPlaceholderText<HTMLInputElement>(/от/i);
    const rangeMaxElement =
      screen.getByPlaceholderText<HTMLInputElement>(/до/i);
    const buttonElement = screen.getByTestId('filters-apply');

    fireEvent.change(rangeMinElement, { target: { value: '1000' } });
    fireEvent.change(rangeMaxElement, { target: { value: '25000' } });

    expect(rangeMinElement.value).toBe('1000');
    expect(rangeMaxElement.value).toBe('25000');

    fireEvent.click(buttonElement);

    expect(handleToggleVisible).toBeCalled();
  });

  it('should match snapshot', () => {
    const handleToggleVisible = vi.fn();
    const container = renderWithProviders(
      <FiltersList toggleVisible={handleToggleVisible} />,
    );

    expect(container).toMatchSnapshot();
  });
});
