import { useState } from 'react';

import { ROOMS_FILTER } from '@modules/Catalog/constants';

import { useTypedSelector } from '@hooks/useTypedSelector';
import { useActions } from '@hooks/useActions';

import { Select } from '@ui/Select/Select';
import { SelectItem } from '@ui/SelectItem/SelectItem';
import { Button } from '@ui/Button/Button';
import { INumberRange, NumberRange } from '@components/NumberRange/NumberRange';

import styles from './FiltersList.module.scss';
import { Form } from '@ui/Form/Form';

interface IFiltersListProps {
  /** Callback to be called when the modal window is closed */
  toggleVisible: () => void;
}

export const FiltersList = ({ toggleVisible }: IFiltersListProps) => {
  const filters = useTypedSelector((state) => state.catalog.filters);
  const { setPrice, setRooms } = useActions();

  const [settings, setSettings] = useState({
    rooms: filters.rooms_like || '',
    price: {
      min: filters.price_gte,
      max: filters.price_lte,
    } as INumberRange,
  });

  const handleFilter = () => {
    const { price, rooms } = settings;

    setPrice({
      min: price.min,
      max: price.max && price.max > 0 ? price.max : undefined,
    });
    setRooms(rooms);

    toggleVisible();
  };

  return (
    <div className={styles.container}>
      <div className={styles.items}>
        <Form label="Количество комнат">
          <Select
            selectedValue={ROOMS_FILTER.find(
              (element) => element.value === settings.rooms,
            )}
            label={
              ROOMS_FILTER.find((element) => element.value === settings.rooms)
                ?.label
            }
            onChange={(item) =>
              setSettings((prev) => ({
                ...prev,
                rooms: item.value as number,
              }))
            }
            placeholder="Выбор комнат"
            className={styles.dropdown}
          >
            {ROOMS_FILTER.map((company) => (
              <SelectItem key={company.value} value={company.value}>
                {company.label}
              </SelectItem>
            ))}
          </Select>
        </Form>

        <Form label="Цена">
          <NumberRange
            postfix="₽"
            min={settings.price.min}
            max={settings.price.max}
            onChange={(newPrice) =>
              setSettings((prev) => ({ ...prev, price: newPrice }))
            }
            className={styles.range}
          />
        </Form>

        <Button
          variant="primary"
          className={styles.button}
          onClick={handleFilter}
          data-testid="filters-apply"
        >
          Применить
        </Button>
      </div>
    </div>
  );
};
