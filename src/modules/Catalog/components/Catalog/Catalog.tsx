import { useTypedSelector } from '@hooks/useTypedSelector';
import { useGetApartmentsQuery } from '../../api/catalogApi';

import { ApartmentMap } from '../ApartmentMap/ApartmentMap';
import { Filters } from '../Filters/Filters';
import { Sidebar } from '../Sidebar/Sidebar';

import styles from './Catalog.module.scss';

export const Catalog = () => {
  const filters = useTypedSelector((state) => state.catalog.filters);

  const { data: apartments, isLoading } = useGetApartmentsQuery(filters, {
    skip: filters.x_gte === 0,
    refetchOnMountOrArgChange: true,
  });

  return (
    <div className={styles.container}>
      <Filters />

      <div className={styles.content}>
        <Sidebar />

        <ApartmentMap placemarks={apartments} isLoading={isLoading} />
      </div>
    </div>
  );
};
