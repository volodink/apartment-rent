import type { Meta, StoryObj } from '@storybook/react';

import { apartmentHandler } from '@tests/server/apartmentHandler';
import { StoryApp } from '@stories/helpers/StoryApp';

import { Catalog } from './Catalog';

/** This module is used to display apartments on the map. */
const meta = {
  title: 'Modules/Catalog',
  component: Catalog,
  parameters: {
    layout: 'padded',
  },
  tags: ['autodocs'],
  decorators: [
    (Story) => (
      <StoryApp>
        <Story />
      </StoryApp>
    ),
  ],
} satisfies Meta<typeof Catalog>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  parameters: {
    msw: apartmentHandler,
  },
};
