import type { Meta, StoryObj } from '@storybook/react';

import { MOCK_DATA_PLACEMARKS } from '@tests/mocks/placemarkMock';

import { StoryProvider } from '@stories/helpers/StoryProvider';

import { ApartmentMap } from './ApartmentMap';

/** This component can be used to display placemarks with apartments */
const meta: Meta<typeof ApartmentMap> = {
  title: 'Modules/Catalog/ApartmentMap',
  component: ApartmentMap,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  args: {
    placemarks: MOCK_DATA_PLACEMARKS,
    isLoading: false,
  },
  decorators: [
    (Story) => (
      <StoryProvider>
        <Story />
      </StoryProvider>
    ),
  ],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  decorators: [
    (Story) => (
      <div className="h-[720px] w-[1280px]">
        <Story />
      </div>
    ),
  ],
};
