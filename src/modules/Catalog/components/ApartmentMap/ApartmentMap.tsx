import { useRef } from 'react';
import ymaps from 'yandex-maps';
import { YMaps, Map } from '@pbe/react-yandex-maps';

import {
  placemarkTransformText,
  clusterTransformText,
} from '@modules/Catalog/utils';

import { IApartmentPlacemark } from '@modules/Catalog/interfaces';

import { useActions } from '@hooks/useActions';
import { useDebounce } from '@hooks/useDebounce';

import { PlacemarksMap } from '@components/PlacemarksMap/PlacemarksMap';

interface IApartmentMapProps {
  /** Sets the displayed apartments on the map */
  placemarks?: IApartmentPlacemark[];
  /** Sets the visibility of data loading */
  isLoading: boolean;
}

export const ApartmentMap = ({
  placemarks = [],
  isLoading,
}: IApartmentMapProps) => {
  const ref = useRef<ymaps.Map>();
  const { setCoordinates, setSelectedApartments } = useActions();

  const handleCoordinatesChange = useDebounce<number[][]>((coordinates) =>
    setCoordinates(coordinates),
  );

  const handlePlacemarkClick = (apartment: IApartmentPlacemark[]) => {
    setSelectedApartments(apartment);
  };

  return (
    <YMaps>
      <Map
        defaultState={{
          center: [55.751574, 37.573856],
          zoom: 10,
        }}
        onLoad={() =>
          handleCoordinatesChange(ref.current?.getBounds() as number[][])
        }
        instanceRef={(obj) => (ref.current = obj)}
        width="100%"
        height="100%"
        modules={['template.filtersStorage']}
      >
        <PlacemarksMap
          data={placemarks}
          isLoading={isLoading}
          displayedMarkValue="price"
          transformMarkText={placemarkTransformText}
          transformClusterText={clusterTransformText}
          onPlacemarkClick={handlePlacemarkClick}
        />
      </Map>
    </YMaps>
  );
};
