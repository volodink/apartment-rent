import clsx from 'clsx';

import { Icon } from '@ui/Icon/Icon';
import { Button } from '@ui/Button/Button';
import { SidebarList } from './SidebarList/SidebarList';

import styles from './Sidebar.module.scss';
import { useActions } from '@hooks/useActions';
import { useTypedSelector } from '@hooks/useTypedSelector';

export const Sidebar = () => {
  const selectedApartments = useTypedSelector(
    (state) => state.catalog.selectedApartments,
  );
  const { resetSelectedApartments } = useActions();

  return (
    <div
      className={clsx(styles.sidebar, {
        [styles.active]: selectedApartments.length,
      })}
      data-testid="catalog-sidebar"
    >
      <Button
        variant="transparent"
        onClick={() => resetSelectedApartments()}
        className={styles.close}
        data-testid="catalog-sidebar-close"
      >
        <Icon name="close" size={24} />
      </Button>

      <SidebarList apartments={selectedApartments} />
    </div>
  );
};
