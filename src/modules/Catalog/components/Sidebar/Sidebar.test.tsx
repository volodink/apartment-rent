import { fireEvent, screen } from '@testing-library/react';

import { MOCK_DATA_APARTMENTS } from '@tests/mocks/apartmentsMock';

import { renderTestApp } from '@tests/helpers/renderTestApp';

import { IQueryParams } from '@modules/Catalog/interfaces';
import { Sidebar } from './Sidebar';

describe('Sidebar', () => {
  it('should render correctly', () => {
    renderTestApp(<Sidebar />);

    const sidebarElement = screen.getByTestId('catalog-sidebar');
    const firstCardElement = screen.queryByText('1016 6th Park');

    expect(sidebarElement.getAttribute('class')).not.toMatch(/active/gi);
    expect(firstCardElement).not.toBeInTheDocument();
  });

  it('should render correctly with selected apartments', () => {
    renderTestApp(<Sidebar />, {
      preloadedState: {
        catalog: {
          selectedApartments: MOCK_DATA_APARTMENTS,
          filters: {} as IQueryParams,
        },
      },
    });

    const sidebarElement = screen.getByTestId('catalog-sidebar');
    const firstCardElement = screen.getByText('1016 6th Park');
    const secondCardElement = screen.getByText('93874 Green Park');

    expect(sidebarElement.getAttribute('class')).toMatch(/active/gi);
    expect(firstCardElement).toBeInTheDocument();
    expect(secondCardElement).toBeInTheDocument();
  });

  it('should selected apartments reset after closing', () => {
    renderTestApp(<Sidebar />, {
      preloadedState: {
        catalog: {
          selectedApartments: MOCK_DATA_APARTMENTS,
          filters: {} as IQueryParams,
        },
      },
    });

    const sidebarElement = screen.getByTestId('catalog-sidebar');
    const firstCardElement = screen.getByText('1016 6th Park');
    const secondCardElement = screen.getByText('93874 Green Park');

    expect(sidebarElement.getAttribute('class')).toMatch(/active/gi);
    expect(firstCardElement).toBeInTheDocument();
    expect(secondCardElement).toBeInTheDocument();

    const closingButton = screen.getByTestId('catalog-sidebar-close');
    fireEvent.click(closingButton);

    expect(sidebarElement.getAttribute('class')).not.toMatch(/active/gi);
    expect(firstCardElement).not.toBeInTheDocument();
    expect(secondCardElement).not.toBeInTheDocument();
  });

  it('should match snapshot', () => {
    const container = renderTestApp(<Sidebar />, {
      preloadedState: {
        catalog: {
          selectedApartments: MOCK_DATA_APARTMENTS,
          filters: {} as IQueryParams,
        },
      },
    });
    expect(container).toMatchSnapshot();
  });
});
