import type { Meta, StoryObj } from '@storybook/react';

import { MOCK_DATA_APARTMENTS } from '@tests/mocks/apartmentsMock';

import { IQueryParams } from '@modules/Catalog/interfaces';

import { StoryApp } from '@stories/helpers/StoryApp';

import { Sidebar } from './Sidebar';

/** This component is used to display a list of apartments.*/
const meta = {
  title: 'Modules/Catalog/Sidebar',
  component: Sidebar,
  parameters: {
    layout: 'padded',
    docs: {
      story: {
        inline: false,
        iframeHeight: 400,
      },
    },
  },
  tags: ['autodocs'],
  decorators: [
    (Story) => (
      <StoryApp
        preloadedState={{
          catalog: {
            selectedApartments: MOCK_DATA_APARTMENTS,
            filters: {} as IQueryParams,
          },
        }}
      >
        <Story />
      </StoryApp>
    ),
  ],
} satisfies Meta<typeof Sidebar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
