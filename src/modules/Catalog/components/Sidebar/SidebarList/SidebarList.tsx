import { IApartment } from '@interfaces/Apartment';

import { Card } from '@components/Card/Card';

import styles from './SidebarList.module.scss';

interface ISidebarListProps {
  /** Sets the list of apartments to display */
  apartments: IApartment[];
}

export const SidebarList = ({ apartments = [] }: ISidebarListProps) => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Список квартир</h1>
      <div className={styles.items}>
        {apartments.map((apartment) => (
          <Card
            key={apartment.id}
            id={apartment.id}
            address={apartment.address}
            image={apartment.image}
            price={apartment.price}
            rooms={apartment.rooms}
            square={apartment.square}
            className={styles.card}
          />
        ))}
      </div>
    </div>
  );
};
