import { screen, RenderResult } from '@testing-library/react';

import { MOCK_DATA_APARTMENTS } from '@tests/mocks/apartmentsMock';

import { renderWithRouter } from '@tests/helpers/renderWithRouter';

import { SidebarList } from './SidebarList';

describe('SidebarList', () => {
  let container: RenderResult;

  beforeEach(() => {
    container = renderWithRouter(
      <SidebarList apartments={MOCK_DATA_APARTMENTS} />,
    );
  });

  it('should render correctly', () => {
    const titleElement = screen.getByText(/Список квартир/i);
    const firstCardElement = screen.getByText('1016 6th Park');
    const secondCardElement = screen.getByText('93874 Green Park');

    expect(titleElement).toBeInTheDocument();
    expect(firstCardElement).toBeInTheDocument();
    expect(secondCardElement).toBeInTheDocument();
  });

  it('should match snapshot', () => {
    expect(container).toMatchSnapshot();
  });
});
