export interface IQueryParams {
  x_gte: number;
  x_lte: number;
  y_gte: number;
  y_lte: number;
  rooms_like?: number | string;
  ceiling_like?: number | string;
  price_gte?: number;
  price_lte?: number;
}
