import { IApartment } from '@interfaces/Apartment';

interface IPlacemarkGeometry {
  type: string;
  coordinates: number[];
}

export interface IApartmentPlacemark extends IApartment {
  geometry: IPlacemarkGeometry;
}
