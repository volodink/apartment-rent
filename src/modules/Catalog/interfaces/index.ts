export type { IQueryParams } from './QueryParams';

export type { IApartmentPlacemark } from './ApartmentPlacemark';
