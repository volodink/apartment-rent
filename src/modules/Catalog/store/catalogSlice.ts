import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IApartment } from '@interfaces/Apartment';
import { INumberRange } from '@components/NumberRange/NumberRange';
import { IQueryParams } from '../interfaces';

const initialState = {
  selectedApartments: [] as IApartment[],
  filters: {
    x_gte: 0,
    x_lte: 0,
    y_gte: 0,
    y_lte: 0,
  } as IQueryParams,
};

export const catalogSlice = createSlice({
  name: 'catalog',
  initialState,
  reducers: {
    setSelectedApartments: (state, action: PayloadAction<IApartment[]>) => {
      state.selectedApartments = action.payload;
    },
    resetSelectedApartments: (state) => {
      state.selectedApartments = [];
    },
    setCoordinates: (state, action: PayloadAction<number[][]>) => {
      state.filters.x_gte = action.payload[0][0];
      state.filters.x_lte = action.payload[1][0];
      state.filters.y_gte = action.payload[0][1];
      state.filters.y_lte = action.payload[1][1];
    },

    setRooms: (state, action: PayloadAction<number | string>) => {
      state.filters.rooms_like = action.payload;
    },

    setCeiling: (state, action: PayloadAction<string>) => {
      state.filters.ceiling_like = action.payload;
    },

    setPrice: (state, action: PayloadAction<INumberRange>) => {
      state.filters.price_gte = action.payload.min;
      state.filters.price_lte = action.payload.max;
    },
  },
});

export const catalogReducer = catalogSlice.reducer;
export const catalogAction = catalogSlice.actions;
