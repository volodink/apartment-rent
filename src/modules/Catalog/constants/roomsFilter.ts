export const ROOMS_FILTER = [
  {
    label: 'Любое',
    value: '',
  },
  {
    label: '1 комната',
    value: 1,
  },
  {
    label: '2 комнаты',
    value: 2,
  },
  {
    label: '3 комнаты',
    value: 3,
  },
  {
    label: '4 комнаты',
    value: 4,
  },
  {
    label: '5 комнат',
    value: 5,
  },
];
