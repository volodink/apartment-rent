export {
  catalogSlice,
  catalogAction,
  catalogReducer,
} from './store/catalogSlice';

export { catalogApi } from './api/catalogApi';

export { Catalog } from './components/Catalog/Catalog';
