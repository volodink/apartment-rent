export const placemarkTransformText = (price: string | number): string => {
  return `${price.toLocaleString()} ₽`;
};
