const formatNumber = (n: number) => {
  if (n < 1e3) return n;
  if (n >= 1e3 && n < 1e6)
    return `${(n / 1e3).toFixed(1).replace(/\.0$/, '')} тыс.`;
  if (n >= 1e6 && n < 1e9)
    return `${(n / 1e6).toFixed(1).replace(/\.0$/, '')} млн.`;
  if (n >= 1e9 && n < 1e12)
    return `${(n / 1e9).toFixed(1).replace(/\.0$/, '')} млрд.`;
  if (n >= 1e12) return `${(n / 1e12).toFixed(1).replace(/\.0$/, '')} млрд.`;
};

export const formatPrice = (max: number, min: number) => {
  let result = '';

  if (min) {
    result += `от ${formatNumber(min)} `;
  }

  if (max) {
    result += `до ${formatNumber(max)}`;
  }

  if (max || min) {
    result += ' ₽';
  }

  return result;
};
