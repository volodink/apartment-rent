import { IApartmentPlacemark } from '../interfaces';

export const clusterTransformText = (data: IApartmentPlacemark[]) => {
  const minPrice = data.sort((a, b) => a.price - b.price)[0].price;

  return `${data.length} от ${minPrice.toLocaleString()} ₽`;
};
