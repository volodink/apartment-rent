import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { IQueryParams } from '../interfaces';
import { IApartmentPlacemark } from '../interfaces/ApartmentPlacemark';

export const catalogApi = createApi({
  reducerPath: 'api/catalog',
  baseQuery: fetchBaseQuery({
    baseUrl: `${import.meta.env.VITE_API_URL}`,
  }),
  endpoints: (build) => ({
    getApartments: build.query<IApartmentPlacemark[], IQueryParams>({
      query: (params) => ({
        url: `/apartments`,
        params,
      }),
      transformResponse: (response: IApartmentPlacemark[]) => {
        return response.map((apartment) => ({
          ...apartment,
          geometry: {
            type: 'Point',
            coordinates: [apartment.x, apartment.y],
          },
        }));
      },
    }),
  }),
});

export const { useGetApartmentsQuery } = catalogApi;
